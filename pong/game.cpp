#include <entt/entt.hpp>
#include <raylib.h>
#include <random>
#include "game.h"
#include <filesystem>

static auto create_pong() -> std::unique_ptr<Game> {
    return std::make_unique<PongGame>();
}

bool PongGame::registered = register_game_creator({create_pong, "Pong"});

namespace pong {
    constexpr auto paddleHeight = 100;
    constexpr auto paddleWidth = 15;
    constexpr auto ballSize = 36;
    constexpr auto paddleOffset = ballSize + 5;
    constexpr auto ballSpeed = 350.0f;
    constexpr auto paddleSpeed = 190.0f;
    constexpr auto centerLineWidth = 8.0f;

    struct Pos {
        float x;
        float y;
    };

    struct Size {
        float w;
        float h;
    };

    struct Controllable {
        int keyMoveUp;
        int keyMoveDown;
        int keyLaunch;
        int gamePad;
        int gamePadUp;
        int gamePadDown;
        int gamePadLaunch;
    };

    struct Velocity {
        float x;
        float y;
    };

    struct Bouncable {
        bool b{};
    };

    struct Paddle {
        int otherScore;
    };

    struct Launchable {
        bool b = {};
    };

    struct Random {
        using distribution = std::uniform_real_distribution<float>;
        std::mt19937 engine;
        distribution dst;

        auto operator()() -> float {
            return dst(engine);
        }
    };

    auto register_player(entt::registry &, int x,
                         // Keyboard controls
                         int keyUp, int keyDown, int keyLaunch,
                         // gamepad controls
                         int gamePad, int gpUp, int gpDown, int gpLaunch
                         ) -> void;

    auto register_ball(entt::registry &) -> void;

    auto render(const entt::registry &) -> void;

    auto update(entt::registry &, float) -> void;

    auto setup_game() -> entt::registry {
        entt::registry registry;

        // Setup randomness
        std::random_device rd;
        std::mt19937 engine(rd());
        auto entity = registry.create();
        Random::distribution dst(0.0f, 1.0f);
        registry.emplace<Random>(entity, engine, dst);

        register_player(registry, paddleOffset, KEY_W, KEY_S, KEY_SPACE, 0, GAMEPAD_BUTTON_LEFT_FACE_UP, GAMEPAD_BUTTON_LEFT_FACE_DOWN, GAMEPAD_BUTTON_RIGHT_FACE_DOWN);
        register_player(registry, winWidth - paddleOffset - paddleWidth, KEY_I, KEY_K, 0, 1, GAMEPAD_BUTTON_LEFT_FACE_UP, GAMEPAD_BUTTON_LEFT_FACE_DOWN, GAMEPAD_BUTTON_RIGHT_FACE_DOWN);
        register_ball(registry);
        return registry;
    }
    auto register_ball(entt::registry &registry) -> void {
        auto entity = registry.create();
        registry.emplace<Pos>(entity, static_cast<float>(winWidth - ballSize) / 2.0f,
                              static_cast<float>(winHeight - ballSize) / 2);
        registry.emplace<Size>(entity, static_cast<float>(ballSize), static_cast<float>(ballSize));
        registry.emplace<Bouncable>(entity);
        registry.emplace<Launchable>(entity);
        registry.emplace<Velocity>(entity, 0.0f, 0.0f);
    }

    auto register_player(entt::registry &registry, int x,
            // Keyboard controls
             int keyUp, int keyDown, int keyLaunch,
            // gamepad controls
             int gamePad, int gpUp, int gpDown, int gpLaunch) -> void {
        auto entity = registry.create();
        registry.emplace<Pos>(entity, static_cast<float>(x), static_cast<float>(winHeight - paddleHeight) / 2.0f);
        registry.emplace<Size>(entity, static_cast<float>(paddleWidth), static_cast<float>(paddleHeight));
        registry.emplace<Velocity>(entity, 0.0f, 0.0f);
        registry.emplace<Controllable>(entity, keyUp, keyDown, keyLaunch, gamePad, gpUp, gpDown, gpLaunch);
        registry.emplace<Paddle>(entity);
    }

    auto render(const entt::registry &registry) -> void {
        BeginDrawing();
        ClearBackground(BLACK);

        DrawRectangle(
                static_cast<int>(winWidth / 2.0f - centerLineWidth / 2.0f),
                static_cast<int>(0),
                static_cast<int>(centerLineWidth),
                winHeight,
                WHITE
        );

        registry.view<const Pos, const Size>().each([](const auto &pos, const auto &size) {
            DrawRectangle(
                    static_cast<int>(pos.x),
                    static_cast<int>(pos.y),
                    static_cast<int>(size.w),
                    static_cast<int>(size.h),
                    WHITE
            );
        });

        registry.view<const Pos, const Paddle>().each([](const Pos &pos, const Paddle &paddle) {
            constexpr auto scoreSize = 64;
            const auto isLeft = pos.x < winWidth / 2.0;
            if (isLeft) {
                // other score is for the right player's score
                auto score = std::to_string(paddle.otherScore);
                DrawText(score.c_str(), (winWidth * 2) / 3, 12, scoreSize, WHITE);
            } else {
                auto score = std::to_string(paddle.otherScore);
                DrawText(score.c_str(), (winWidth * 1) / 3, 12, scoreSize, WHITE);
            }
        });

        EndDrawing();
    }
}

PongGame::PongGame() : registry(pong::setup_game()) {
    const auto assetPath = std::filesystem::path{"assets"};

    auto launchSoundFile = assetPath / "pong-ball-launch.wav";
    launchSound = LoadSound(launchSoundFile.c_str());
    auto bounceSoundFile = assetPath / "pong-bounce.wav";
    bounceSound = LoadSound(bounceSoundFile.c_str());
    auto goalSoundFile = assetPath / "pong-goal.wav";
    goalSound = LoadSound(goalSoundFile.c_str());
    auto musicWave = assetPath / "pong-song.wav";
    bgMusic = LoadMusicStream(musicWave.c_str());
    PlayMusicStream(bgMusic);
    SetMusicVolume(bgMusic, 0.6f);
}

void PongGame::update() {
    using namespace pong;

    bool launch = false;

    auto delta = GetFrameTime();
    UpdateMusicStream(bgMusic);

    registry.view<Velocity, const Controllable>().each(
            [&launch](const auto entity, auto &velocity, const Controllable &c) {
                velocity.y = 0.0f;
                if (IsKeyDown(c.keyMoveUp) || IsGamepadButtonDown(c.gamePad, c.gamePadUp)) {
                    velocity.y -= paddleSpeed;
                }
                if (IsKeyDown(c.keyMoveDown) || IsGamepadButtonDown(c.gamePad, c.gamePadDown)) {
                    velocity.y += paddleSpeed;
                }
                if (IsKeyPressed(c.keyLaunch) || IsGamepadButtonPressed(c.gamePad, c.gamePadLaunch)) {
                    launch = true;
                }
            });

    registry.view<const Pos, Velocity, const Launchable>().each(
            [&](const auto entity, auto &velocity, const Launchable &l) {
                if (abs(velocity.x) > std::numeric_limits<float>::epsilon() ||
                    abs(velocity.y) > std::numeric_limits<float>::epsilon())
                    return;

                auto randomEntity = registry.view<Random>().front();
                auto &random = registry.get<Random>(randomEntity);
                if (launch) {
                    auto angle = random() * PI / 2.0f + 3.0f * PI / 4.0f;

                    // 50% chance to change initial direction
                    if (random() < 0.5) {
                        angle += PI;
                    }
                    velocity.x = ballSpeed * cos(angle);
                    velocity.y = ballSpeed * sin(angle);
                    PlaySound(launchSound);
                }
            });

    registry.view<Pos, Velocity, const Size, const Paddle>().each(
            [delta](const auto entity, auto &pos, auto &vel, auto &size, auto &b) {
                pos.x = std::max(0.0f, std::min(static_cast<float>(winWidth) - size.w, pos.x + vel.x * delta));
                pos.y = std::max(0.0f, std::min(static_cast<float>(winHeight) - size.h, pos.y + vel.y * delta));
            });

    registry.view<Pos, Velocity, const Size, const Bouncable>().each(
            [&, delta](const auto entity, auto &pos, auto &vel, auto &size, auto &b) {
                pos.x += vel.x * delta;
                pos.y += vel.y * delta;

                const auto leftPlane = pos.x;
                const auto rightPlane = pos.x + size.w;
                const auto topPlane = pos.y;
                const auto bottomPlane = pos.y + size.h;

                if (leftPlane <= 0) {
                    vel.x = abs(vel.x);
                } else if (rightPlane >= winWidth) {
                    vel.x = -abs(vel.x);
                }
                if (topPlane <= 0) {
                    vel.y = abs(vel.y);
                    PlaySound(bounceSound);
                } else if (bottomPlane >= winHeight) {
                    vel.y = -abs(vel.y);
                    PlaySound(bounceSound);
                }

                registry.view<Pos, const Size, Paddle>()
                        .each([&, leftPlane, rightPlane, topPlane, bottomPlane](
                                const auto paddleEntity, auto &paddlePos, auto &paddleSize, Paddle &paddle) {
                            if (entity == paddleEntity) return;

                            const auto paddleLeft = paddlePos.x;
                            const auto paddleMid = paddlePos.x + paddleSize.w / 2.0f;
                            const auto paddleRight = paddlePos.x + paddleSize.w;
                            const auto isLeft = paddlePos.x < winWidth / 2.0;
                            const auto paddleTop = paddlePos.y;
                            const auto paddleBottom = paddlePos.y + paddleSize.h;

                            if ((isLeft && rightPlane < paddleLeft) || (!isLeft && leftPlane > paddleRight)) {
                                ++paddle.otherScore;
                                pos.x = winWidth / 2.0f - size.w / 2.0f;
                                pos.y = winHeight / 2.0f - size.h / 2.0f;
                                vel.x = 0.0f;
                                vel.y = 0.0f;
                                PlaySound(goalSound);
                                return;
                            }

                            if (bottomPlane < paddleTop || topPlane > paddleBottom) return;

                            if (isLeft) {
                                if (leftPlane <= paddleRight && leftPlane >= paddleMid) {
                                    vel.x = abs(vel.x);
                                    PlaySound(bounceSound);
                                }
                            } else {
                                if (rightPlane >= paddleLeft && leftPlane <= paddleMid) {
                                    vel.x = -abs(vel.x);
                                    PlaySound(bounceSound);
                                }
                            }
                        });
            });
    pong::render(registry);
}

PongGame::~PongGame() {
    StopSound(bounceSound);
    StopSound(launchSound);
    StopSound(goalSound);
    StopMusicStream(bgMusic);

    UnloadSound(bounceSound);
    UnloadSound(launchSound);
    UnloadSound(goalSound);
    UnloadMusicStream(bgMusic);
}
