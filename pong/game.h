#pragma once

#include "../common/common.h"
#include <entt/entt.hpp>

class PongGame : public Game {
public:
    PongGame();

    ~PongGame() override;

    void update() override;
private:
    entt::registry registry;

    static bool registered;
    Sound bounceSound;
    Sound launchSound;
    Sound goalSound;
    Music bgMusic;
};
