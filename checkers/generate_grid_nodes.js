const fs = require('fs');

const BLACK = 0x1 << 1;
const WHITE = 0x1 << 2;
const KING = 0x1 << 3;
const BLACK_KING_ZONE = 0x1 << 4;
const WHITE_KING_ZONE = 0x1 << 5;
const COLOR_BITS = BLACK | WHITE;
const KING_ZONE_BITS = BLACK_KING_ZONE | WHITE_KING_ZONE;
const NO_COLOR = 0

let board = [
    [BLACK | WHITE_KING_ZONE, 0, BLACK | WHITE_KING_ZONE, 0, BLACK | WHITE_KING_ZONE, 0, BLACK | WHITE_KING_ZONE, 0 ],
    [0, BLACK, 0, BLACK, 0, BLACK, 0, BLACK ],
    [BLACK, 0, BLACK, 0, BLACK, 0, BLACK, 0 ],
    [0, 1, 0, 1, 0, 1, 0, 1 ],
    [1, 0, 1, 0, 1, 0, 1, 0 ],
    [0, WHITE, 0, WHITE, 0, WHITE, 0, WHITE ],
    [WHITE, 0, WHITE, 0, WHITE, 0, WHITE, 0 ],
    [0, WHITE| BLACK_KING_ZONE, 0, WHITE | BLACK_KING_ZONE, 0, WHITE | BLACK_KING_ZONE, 0, WHITE | BLACK_KING_ZONE ],
]

function set_circle_test_case() {
    board = [
        [WHITE_KING_ZONE, 0, WHITE_KING_ZONE, 0, WHITE_KING_ZONE, 0, WHITE_KING_ZONE, 0 ],
        [0, 1, 0, 1, 0, 1, 0, WHITE ],
        [1, 0, WHITE, 0, WHITE, 0, 1, 0 ],
        [0, 1, 0, 1, 0, 1, 0, 1 ],
        [1, 0, WHITE, 0, WHITE, 0, 1, 0 ],
        [0, 1, 0, 1, 0, 1, 0, 1 ],
        [1, 0, 1, 0, WHITE, 0, BLACK, 0 ],
        [0, BLACK_KING_ZONE, 0, BLACK_KING_ZONE, 0, BLACK_KING_ZONE, 0, BLACK_KING_ZONE ],
    ]
}

// set_circle_test_case()

function to_index(x, y) {
    return Math.floor(x/2) + y * 4
}

function Node(x, y, startColor, kingZone) {
    this.index = to_index(x, y)
    this.x = x
    this.y = y
    this.startColor = startColor
    this.kingZone = kingZone
}

const nodes = []

const validStartColors = [BLACK, WHITE]

function to_valid_color(color) {
    const color_bits = color & COLOR_BITS;
    if (validStartColors.indexOf(color_bits) >= 0) {
        return color_bits
    }
    return NO_COLOR
}

function to_king_zone(cell) {
    const zone = cell & KING_ZONE_BITS;
    if (zone === BLACK_KING_ZONE || zone === WHITE_KING_ZONE) {
        return zone;
    }
    return 0;
}

board.forEach((row, y) => {
    row.forEach((cell, x) => {
        if (cell === 0) return;

        nodes.push(new Node(x, y, to_valid_color(cell), to_king_zone(cell)));
    })
})

const adjacencyGrid = Array(32).fill(1).map(_ => Array(32).fill(0))

board.forEach((row, y) => {
    row.forEach((cell, x) => {
        if (cell === 0) return;

        const index = to_index(x, y)
        const siblings = [[-1, -1, BLACK], [-1, 1, WHITE], [1, 1, WHITE], [1, -1, BLACK]]
            .map(([xDelta, yDelta, color]) => [x + xDelta, y + yDelta, color])
            .filter(([xSibling, ySibling, _color]) => xSibling >= 0 && ySibling >= 0 && xSibling < 8 && ySibling < 8)
            .map(([xSibling, ySibling, color]) => [to_index(xSibling, ySibling), color]);
        siblings
            .forEach(([siblingIndex, color]) => adjacencyGrid[siblingIndex][index] = color)
    })
})

function logGraphviz() {
    console.log(`digraph BoardGraph {`)
    for (const node of nodes) {
        let color = 'black'
        let fillColor = 'white'
        let shape = 'ellipse'

        if (node.startColor === BLACK) {
            color = 'white'
            fillColor = 'black'
        } else if (node.startColor === WHITE) {
            color = 'white'
            fillColor = 'red'
        }

        if (node.kingZone === BLACK_KING_ZONE) {
            shape = 'star';
        } else if (node.kingZone === WHITE_KING_ZONE) {
            shape = 'box'
        }

        console.log(`${node.index}[label="${node.x}, ${node.y}";shape="${shape}";style="filled";fillcolor="${fillColor}";fontcolor="${color}"];`)
    }

    for (const node of nodes) {
        adjacencyGrid[node.index].forEach((borderColor, otherNodeIndex) => {
            if (!borderColor) return;
            console.log(`${node.index} -> ${otherNodeIndex}[color="${borderColor === BLACK ? 'black' : 'red'}"];`)
        })
    }
    console.log('}')
}

logGraphviz()

function generateNodesOutput() {
    const nodeStrs = []
    for (const node of nodes) {
        let flags = [];
        if (node.kingZone) {
            flags.push(node.kingZone === BLACK_KING_ZONE ? 'NodeFlags::NODE_BLACK_KING_ZONE' : 'NodeFlags::NODE_WHITE_KING_ZONE');
        }
        if (node.startColor) {
            flags.push(node.startColor === BLACK ? 'NodeFlags::NODE_BLACK_START' : 'NodeFlags::NODE_WHITE_START');
        }
        const flagStr = flags.length ? flags.join('|') : '0x0';
        nodeStrs.push(`\n\t\tNode{${flagStr}, ${node.index}, ${node.x},${node.y}}`);
    }
    return nodeStrs.join(',') + '\n\t';
}

function generateEdgesOutput() {
    const edgeRows = [];
    for (const row of adjacencyGrid) {
        const rowNodes = [];
        for (const edgeColor of row) {
            if (!edgeColor) {
                rowNodes.push('0x0');
                continue;
            }

            rowNodes.push((edgeColor === BLACK ? 'EdgeFlags::EDGE_BLACK' : 'EdgeFlags::EDGE_WHITE') + '|EdgeFlags::EDGE_KING');
        }
        edgeRows.push(`\n\t\tstd::array<uint8_t, 32>{${rowNodes.join(',')}}`);
    }
    return edgeRows.join(',') + '\n\t';
}

const outStr = `
#pragma once

///////////////////////////////////////////
/// AUTOGENERATED! DO NOT EDIT MANUALLY ///
///////////////////////////////////////////

#include <array>

namespace checkers::board {
    enum NodeFlags {
        NODE_BLACK_START = 0x1,
        NODE_WHITE_START = 0x2,
        NODE_BLACK_KING_ZONE = 0x4,
        NODE_WHITE_KING_ZONE = 0x8,
        
        NODE_PLAIN = 0x0,
    };
    
    enum EdgeFlags {
        EDGE_BLACK = 0x1,
        EDGE_WHITE = 0x2,
        EDGE_KING = 0x4,

        EDGE_UNKNOWN = 0x0,
    };
    
    struct Node {
        uint8_t flags;
        uint8_t index;
        uint8_t x;
        uint8_t y;
    };

    constexpr auto nodes = std::array{${generateNodesOutput()}};
    constexpr auto edges = std::array{${generateEdgesOutput()}};
}
`

fs.writeFileSync('board_generated.hpp', outStr, 'utf-8');
