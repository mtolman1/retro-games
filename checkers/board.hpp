#pragma once

#include "board_generated.hpp"
#include <vector>
#include <map>

namespace checkers {
    using NodeId = uint8_t;

    enum PieceBoardFlags : uint8_t {
        PIECE_BLACK = 0x1,
        PIECE_WHITE = 0x2,
        PIECE_KING = 0x4,

        PIECE_COLOR = PIECE_BLACK | PIECE_WHITE
    };

    struct Piece {
        NodeId nodeId;
        uint8_t flags;

        Piece() = default;
        Piece(NodeId node, uint8_t flags) : nodeId(node), flags(flags) {}
    };

    struct Move {
        std::shared_ptr<Piece> pieceBeingMoved;
        std::vector<NodeId> spaces;
        std::set<std::shared_ptr<Piece>> piecesJumped;
    };

    using Pieces = std::map<NodeId, std::shared_ptr<Piece>>;

    namespace board {
        inline auto neighbors_of(const Node& node, const std::shared_ptr<Piece>& piece) -> std::vector<NodeId> {
            const auto& nodeEdges = edges[node.index];
            std::vector<NodeId> result{};
            for (NodeId neighborId = 0; neighborId < nodeEdges.size(); ++neighborId) {
                if (nodeEdges[neighborId] & piece->flags) {
                    result.emplace_back(neighborId);
                }
            }
            return result;
        }
    }
}
