#include "../common/common.h"
#include <array>
#include <raylib.h>
#include <set>
#include <stack>
#include <filesystem>
#include "board.hpp"

namespace checkers {
    constexpr auto square_padding = 5;
    constexpr auto piece_select_border = 5;
    constexpr auto piece_border = 5;
    constexpr auto piece_radius = 25;
    constexpr auto grid_length = 8;
    constexpr auto square_size = std::max(piece_select_border, piece_radius) * 2 + piece_border * 2 + square_padding * 2;
    constexpr auto piece_center_offset = square_size / 2;
    constexpr auto board_grid_length = grid_length * square_size;
    constexpr auto board_margin = 15;
    constexpr auto board_size = board_margin * 2 + board_grid_length;
    constexpr auto board_left = (winWidth / 2) - (board_size / 2);
    constexpr auto board_top = (winHeight / 2) - (board_size / 2);
    constexpr auto board_grid_left = board_left + board_margin;
    constexpr auto board_grid_top = board_top + board_margin;
    constexpr auto move_cool_down = 2;
    constexpr auto anim_move_time = 0.5;

    static_assert(board_size < winHeight && board_size < winWidth, "Checkerboard is too big to fit on screen!");

    enum class GameState {
        PLAYING,
        WHITE_WINS,
        BLACK_WINS
    };

    class Checkers : public Game {
    public:
        Checkers();
        ~Checkers() override;
        void update() override {
            UpdateMusicStream(bgMusic);
            ++framesSinceMove;
            update_game();
            render();
        }
    private:
        auto render() -> void;
        auto update_game() -> void;
        auto find_moves() -> void;
        auto make_move() -> void;

        [[nodiscard]] auto gamepad_for_cur_player() const -> int {
            return isBlackTurn ? 0 : 1;
        }

        auto next_turn() -> void {
            // Check if we won
            const auto opponentPieceFlag = isBlackTurn ? PIECE_WHITE : PIECE_BLACK;
            bool foundOpponentPiece = false;
            for (const auto& [_node, piece] : pieces) {
                if (piece->flags & opponentPieceFlag) {
                    foundOpponentPiece = true;
                    break;
                }
            }

            // If we took the last opponent piece, then we win!
            if (!foundOpponentPiece) {
                if (isBlackTurn) {
                    gameState = GameState::BLACK_WINS;
                }
                else {
                    gameState = GameState::WHITE_WINS;
                }
                PlaySound(victorySound);
                return;
            }

            // Otherwise, switch sides
            isBlackTurn = !isBlackTurn;
            selectedIndex = 0;
            find_moves();

            // If the other player can't move, then
            if (currentMoves.empty()) {
                // we win
                if (isBlackTurn) {
                    gameState = GameState::WHITE_WINS;
                }
                else {
                    gameState = GameState::BLACK_WINS;
                }
                PlaySound(victorySound);
            }
        }

        [[nodiscard]] auto player_did_input_move_left() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }

            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_LEFT_FACE_LEFT)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT);
        }

        [[nodiscard]] auto player_did_input_game_reset() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }
            return IsKeyPressed(KEY_R) || IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN) || IsGamepadButtonPressed(1, GAMEPAD_BUTTON_RIGHT_FACE_DOWN);
        }

        [[nodiscard]] auto player_did_input_move_right() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }
            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_LEFT_FACE_RIGHT)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT);
        }

        [[nodiscard]] auto player_did_input_make_move() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }
            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_RIGHT_FACE_DOWN)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_SPACE) || IsKeyPressed(KEY_ENTER);
        }

        static bool registered;
        Pieces pieces;
        size_t selectedIndex;
        std::vector<Move> currentMoves;
        bool isBlackTurn = true;
        GameState gameState = GameState::PLAYING;
        int framesSinceMove = 0;

        std::shared_ptr<Piece> animatedPiece = nullptr;
        std::vector<NodeId> animatedLocations = {};
        std::set<std::shared_ptr<Piece>> animatedRemove = {};
        size_t animIndex = 0;
        double animTime = 0.0;
        bool animSoundPlayed = false;

        Sound changeSelectSound;
        Sound moveJumpSound;
        Sound getKingedSound;
        Sound moveStandardSound;
        Sound victorySound;
        Music bgMusic;
    };

    auto get_starting_pieces() -> Pieces {
        auto pieces = Pieces{};
        for (const auto& node : board::nodes) {
            if (node.flags & board::NODE_BLACK_START) {
                pieces[node.index] = std::make_shared<Piece>(node.index, PIECE_BLACK);
            }
            else if (node.flags & board::NODE_WHITE_START) {
                pieces[node.index] = std::make_shared<Piece>(node.index, PIECE_WHITE);
            }
        }
        return pieces;
    };

    auto piece_fill_color(const std::shared_ptr<Piece>& piece) -> Color {
        return piece->flags & PIECE_WHITE ? WHITE : BLACK;
    }

    auto piece_king_line_color(const std::shared_ptr<Piece>& piece) -> Color {
        return piece->flags & PIECE_WHITE ? BLACK : WHITE;
    }

    auto piece_border_color(const std::shared_ptr<Piece>& piece) -> Color {
        return BLACK;
    }

    auto piece_jumped_fill_color(const std::shared_ptr<Piece>& piece) -> Color {
        auto fillColor = piece_fill_color(piece);
        auto [hue, saturation, value] = ColorToHSV(fillColor);
        if (piece->flags & PIECE_WHITE) {
            value *= 0.8;
        }
        else {
            value = 0.3;
        }
        return ColorFromHSV(hue, saturation, value);
    }

    auto piece_move_fill_color(const std::shared_ptr<Piece>& piece) -> Color {
        return piece->flags & PIECE_WHITE ? GRAY : DARKGRAY;
    }

    struct JumpSearch {
        uint8_t jumpFromNode;
        uint8_t jumpedNode;
        std::set<std::shared_ptr<Piece>> jumpedPieces = {};
        std::vector<NodeId> currentJumpChain = {};
    };
}

bool checkers::Checkers::registered = register_game_creator({[]() { return std::make_unique<checkers::Checkers>(); }, "Checkers"});

checkers::Checkers::Checkers() : pieces(get_starting_pieces()), selectedIndex(0) {
    animTime = GetTime();
    find_moves();

    auto assetDir = std::filesystem::path{"assets"};

    auto changeSelectSoundFile = assetDir / "checkers" / "change-select.wav";
    changeSelectSound = LoadSound(changeSelectSoundFile.c_str());
    auto moveJumpSoundFile = assetDir / "checkers" / "move-jump.wav";
    moveJumpSound = LoadSound(moveJumpSoundFile.c_str());
    auto getKingedSoundFile = assetDir / "checkers" / "move-king.wav";
    getKingedSound = LoadSound(getKingedSoundFile.c_str());
    auto moveStandardSoundFile = assetDir / "checkers" / "move-no-jump.wav";
    moveStandardSound = LoadSound(moveStandardSoundFile.c_str());
    auto victorySoundFile = assetDir / "checkers" / "victory.wav";
    victorySound = LoadSound(victorySoundFile.c_str());
    auto bgMusicFile = assetDir / "checkers" / "song.wav";
    bgMusic = LoadMusicStream(bgMusicFile.c_str());
    PlayMusicStream(bgMusic);
    SetMusicVolume(bgMusic, 0.25f);
}

checkers::Checkers::~Checkers() {
    StopSound(changeSelectSound);
    StopSound(moveJumpSound);
    StopSound(getKingedSound);
    StopSound(moveStandardSound);
    StopSound(victorySound);
    StopMusicStream(bgMusic);

    UnloadSound(changeSelectSound);
    UnloadSound(moveJumpSound);
    UnloadSound(getKingedSound);
    UnloadSound(moveStandardSound);
    UnloadSound(victorySound);
    UnloadMusicStream(bgMusic);
}

constexpr auto cell_to_2d(int x, int y) -> std::tuple<int, int> {
    return {checkers::board_grid_left + x * checkers::square_size, checkers::board_grid_top + y * checkers::square_size};
}

constexpr auto cell_to_2d_center(int x, int y) -> std::tuple<int, int> {
    auto [topLeftX, topLeftY] = cell_to_2d(x, y);
    return {topLeftX + checkers::piece_center_offset, topLeftY + checkers::piece_center_offset};
}

void checkers::Checkers::update_game() {
    if (animatedPiece) {
        return;
    }
    if (gameState != GameState::PLAYING) {
        if (player_did_input_game_reset()) {
            PlaySound(changeSelectSound);
            framesSinceMove = 0;
            pieces = get_starting_pieces();
            isBlackTurn = true;
            gameState = GameState::PLAYING;

            find_moves();
        }
        return;
    }
    if (player_did_input_move_left()) {
        framesSinceMove = 0;
        PlaySound(changeSelectSound);
        if (selectedIndex <= 0) {
            selectedIndex = currentMoves.size() - 1;
        } else {
            --selectedIndex;
        }
    } else if (player_did_input_move_right()) {
        framesSinceMove = 0;
        PlaySound(changeSelectSound);
        selectedIndex = (selectedIndex + 1) % currentMoves.size();
    } else if (player_did_input_make_move()) {
        framesSinceMove = 0;
        make_move();
        find_moves();
    }
}

static void draw_crown_at(int x, int y, Color color) {
    DrawLineEx({static_cast<float>(x - 12), static_cast<float>(y - 10)},
               {static_cast<float>(x - 8), static_cast<float>(y + 10)},
               5, color);
    DrawLineEx({static_cast<float>(x - 10), static_cast<float>(y + 10)},
               {static_cast<float>(x + 10), static_cast<float>(y + 10)},
               5, color);
    DrawLineEx({static_cast<float>(x + 8), static_cast<float>(y + 10)},
               {static_cast<float>(x + 12), static_cast<float>(y - 10)},
               5, color);
    DrawLineEx({static_cast<float>(x), static_cast<float>(y - 10)},
               {static_cast<float>(x), static_cast<float>(y - 5)},
               5, color);

    DrawLineEx({static_cast<float>(x - 9), static_cast<float>(y - 2)},
               {static_cast<float>(x + 9), static_cast<float>(y - 2)},
               5, color);
}

static void draw_piece_at(int x, int y, const std::shared_ptr<checkers::Piece>& piece) {
    DrawCircle(x, y, checkers::piece_radius, checkers::piece_fill_color(piece));
    if (piece->flags & checkers::PIECE_KING) {
        draw_crown_at(x, y, checkers::piece_king_line_color(piece));
    }
}

void checkers::Checkers::render() {
    BeginDrawing();
    ClearBackground(GRAY);

    DrawRectangle(board_left, board_top, board_size, board_size, DARKGRAY);

    for (auto x = 0; x < grid_length; ++x) {
        for (auto y = 0; y < grid_length; ++y) {
            auto [xPixels, yPixels] = cell_to_2d(x, y);
            DrawRectangle(xPixels, yPixels, square_size, square_size, (x + y) % 2 == 1 ? BLACK : RAYWHITE);
        }
    }

    const auto& nodes = board::nodes;
    for (const auto& [nodeId, piece] : pieces) {
        if (!piece || piece == animatedPiece) continue;
        const auto& node = nodes[nodeId];
        auto [x, y] = cell_to_2d_center(node.x, node.y);
        // Border is opposite color for legibility
        DrawCircle(x, y, piece_radius + piece_border, piece_border_color(piece));
        draw_piece_at(x, y, piece);
    }

    if (selectedIndex < currentMoves.size() && !animatedPiece) {
        // Draw current move information
        const auto& curMove = currentMoves[selectedIndex];
        const auto& node = board::nodes[curMove.pieceBeingMoved->nodeId];
        auto [x, y] = cell_to_2d_center(node.x, node.y);

        // draw select border
        DrawCircle(x, y, piece_radius + piece_select_border, GREEN);
        // redraw piece since we drew our select border on top
        draw_piece_at(x, y, curMove.pieceBeingMoved);

        for (const auto& jumped : curMove.piecesJumped) {
            const auto& jumpedNode = board::nodes[jumped->nodeId];
            auto [jumpX, jumpY] = cell_to_2d_center(jumpedNode.x, jumpedNode.y);
            DrawCircle(jumpX, jumpY, piece_radius, piece_jumped_fill_color(jumped));
            if (jumped->flags & PIECE_KING) {
                draw_crown_at(jumpX, jumpY, piece_king_line_color(jumped));
            }
        }

        for (const auto& square : curMove.spaces) {
            const auto& sqNode = board::nodes[square];
            auto [sqX, sqY] = cell_to_2d_center(sqNode.x, sqNode.y);
            DrawCircle(sqX, sqY, piece_radius, piece_move_fill_color(curMove.pieceBeingMoved));
        }
    }

    if (animatedPiece) {
        auto origNode = animatedPiece->nodeId;
        auto orig = board::nodes[origNode];
        auto targetNodeId = animatedLocations[animIndex];
        auto target = board::nodes[targetNodeId];
        auto [origX, origY] = cell_to_2d_center(orig.x, orig.y);
        auto [targetX, targetY] = cell_to_2d_center(target.x, target.y);
        auto time = std::min(2.0, (GetTime() - animTime) / anim_move_time);
        auto deltaTime = std::min(1.0, time);
        auto xDelta = (targetX - origX) * deltaTime;
        auto yDelta = (targetY - origY) * deltaTime;
        int x = static_cast<int>(xDelta) + origX;
        int y = static_cast<int>(yDelta) + origY;
        DrawCircle(x, y, piece_radius + piece_border, piece_border_color(animatedPiece));
        draw_piece_at(x, y, animatedPiece);

        if (time >= 1 && !animSoundPlayed) {
            animSoundPlayed = true;
            PlaySound(moveStandardSound);
        }

        if (time >= 2.0) {
            animatedPiece->nodeId = targetNodeId;
            pieces.erase(origNode);
            pieces[targetNodeId] = animatedPiece;
            animTime = GetTime();
            ++animIndex;
            animSoundPlayed = false;
            if (animIndex >= animatedLocations.size()) {
                animatedLocations.clear();
                animIndex = 0;

                // If this is a king zone...
                if (((target.flags >> 2) & animatedPiece->flags) && !(animatedPiece->flags & PIECE_KING)) {
                    // Make sure we king the piece
                    animatedPiece->flags |= PIECE_KING;
                    PlaySound(getKingedSound);
                }

                // Remove jumped pieces
                std::for_each(animatedRemove.begin(),animatedRemove.end(), [&](const auto& piece) {
                    pieces.erase(piece->nodeId);
                });
                animatedRemove.clear();

                animatedPiece = nullptr;
                next_turn();
            }
        }
    }

    constexpr auto fontSize = 46;
    if (gameState == GameState::WHITE_WINS) {
        DrawText("WHITE WINS!", 1, 1, fontSize, WHITE);
    }
    else if (gameState == GameState::BLACK_WINS) {
        DrawText("BLACK WINS!", 1, 1, fontSize, WHITE);
    }
    else if (isBlackTurn) {
        DrawText("Black's Turn", 1, 1, fontSize, WHITE);
    }
    else {
        DrawText("White's Turn", 1, 1, fontSize, WHITE);
    }

    if (gameState != GameState::PLAYING) {
        DrawText("Press 'R' to play again!", 1, winHeight - fontSize, fontSize, WHITE);
    }

    EndDrawing();
}

auto checkers::Checkers::make_move() -> void {
    auto move = currentMoves[selectedIndex];

    auto oldNode = move.pieceBeingMoved->nodeId;

    animatedPiece = move.pieceBeingMoved;
    animatedLocations = move.spaces;
    animIndex = 0;
    animatedRemove = move.piecesJumped;
    animTime = GetTime();
    animSoundPlayed = false;
}

auto checkers::Checkers::find_moves() -> void {
    auto moves = std::vector<Move>{};
    int minJumps = 0;
    for (const auto& [curNodeId, piece] : pieces) {
        auto curPlayerFlag = isBlackTurn ? PIECE_BLACK : PIECE_WHITE;
        if (!piece || !(piece->flags & curPlayerFlag)) {
            continue;
        }
        const auto& node = board::nodes[curNodeId];
        const auto color = piece->flags & PIECE_COLOR;

        const auto neighbors = board::neighbors_of(node, piece);
        for (const auto& nodeId : neighbors) {
            if (pieces.contains(nodeId) && pieces.at(nodeId) != nullptr) {
                const auto& neighborPiece = pieces.at(nodeId);

                // Cannot jump over own pieces
                if ((neighborPiece->flags & PIECE_COLOR) == color) {
                    continue;
                }

                const auto& searchNode = board::nodes[nodeId];

                auto xDelta = searchNode.x - node.x;
                auto yDelta = searchNode.y - node.y;

                // See if we can find an empty space on the other side
                const auto searchNeighbors = board::neighbors_of(searchNode, piece);
                for (const auto& jumpLoc : searchNeighbors) {
                    // Check to make sure we're in a straight line
                    const auto& jumpNode = board::nodes[jumpLoc];
                    auto searchXDelta = jumpNode.x - searchNode.x;
                    auto searchYDelta = jumpNode.y - searchNode.y;
                    if (searchXDelta != xDelta || searchYDelta != yDelta) {
                        continue;
                    }

                    // If the jump space is occupied then we can't jump to that space
                    if (pieces.contains(jumpLoc) && pieces.at(jumpLoc) != nullptr) {
                        break;
                    }

                    // We found a jump! Make sure we record it on our stack
                    minJumps = std::max(minJumps, 1);
                    if (minJumps == 1) {
                        moves.emplace_back(Move{
                                piece,
                                {jumpLoc},
                                {neighborPiece}
                        });
                    }

                    // Now search for chain jumps
                    auto chainJumps = std::stack<JumpSearch>{};

                    for (const auto jumpNeighbor : board::neighbors_of(jumpNode, piece)) {
                        chainJumps.push(JumpSearch{jumpLoc, jumpNeighbor, {neighborPiece}, {jumpLoc}});
                    }

                    while (!chainJumps.empty()) {
                        auto nextJump = std::move(chainJumps.top());
                        chainJumps.pop();

                        auto njFromNode = nextJump.jumpFromNode;
                        auto njJumpedNode = nextJump.jumpedNode;

                        // If there isn't an opponent piece at the jumped node, just continue on
                        if (!pieces.contains(njJumpedNode) || pieces.at(njJumpedNode) == nullptr || (pieces.at(njJumpedNode)->flags & PIECE_COLOR) == color) {
                            continue;
                        }

                        // If we've jumped that piece already, skip
                        if (nextJump.jumpedPieces.contains(pieces.at(njJumpedNode))) {
                            continue;
                        }

                        auto njNode = board::nodes[njJumpedNode];

                        auto njXDelta = njNode.x - board::nodes[njFromNode].x;
                        auto njYDelta = njNode.y - board::nodes[njFromNode].y;

                        for (const auto njNeighbor : board::neighbors_of(njNode, piece)) {
                            auto njNeighborXDelta = board::nodes[njNeighbor].x - njNode.x;
                            auto njNeighborYDelta = board::nodes[njNeighbor].y - njNode.y;
                            if (njNeighborXDelta != njXDelta || njNeighborYDelta != njYDelta) {
                                continue;
                            }

                            // If the jump space is occupied then we can't jump tp that space
                            if (pieces.contains(njNeighbor) && pieces.at(njNeighbor) != nullptr) {
                                break;
                            }

                            nextJump.currentJumpChain.emplace_back(njNeighbor);
                            nextJump.jumpedPieces.insert(pieces.at(njJumpedNode));
                            if (nextJump.jumpedPieces.size() >= minJumps) {
                                moves.push_back(Move{
                                        piece,
                                        nextJump.currentJumpChain,
                                        nextJump.jumpedPieces
                                });
                                minJumps = nextJump.jumpedPieces.size();
                            }

                            for (const auto deeperNeighbor : board::neighbors_of(board::nodes[njNeighbor], piece)) {
                                chainJumps.push(JumpSearch{
                                    njNeighbor,
                                    deeperNeighbor,
                                    nextJump.jumpedPieces,
                                    nextJump.currentJumpChain
                                });
                            }
                        }
                    }
                }
            }
            else if (!minJumps) {
                moves.emplace_back(Move{
                    piece,
                    {nodeId},
                    {}
                });
            }
        }
    }

    moves.erase(std::remove_if(moves.begin(), moves.end(), [minJumps](const Move& m) {
        return m.piecesJumped.size() < minJumps;
    }), moves.end());

    currentMoves = std::move(moves);
}
