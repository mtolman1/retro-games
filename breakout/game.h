#pragma once

#include "../common/common.h"
#include "../common/input_handler.h"
#include <entt/entt.hpp>
#include <raylib.h>

class BreakoutGame : public Game {
public:
    enum class InputEvents {
        MOVE,
        LAUNCH,
        RESET,
    };

    BreakoutGame();
    void update() override;
    ~BreakoutGame() override;
private:
    entt::registry registry;
    static bool registered;
    Sound launchSound;
    Sound blockBreakSound;
    Sound blockBounceSound;
    Sound paddleBounceSound;
    Sound ballDieSound;
    Sound gameLostSound;
    Sound gameWonSound;
    Music bgMusic;

    input::InputHandler<InputEvents> inputHandler;
};
