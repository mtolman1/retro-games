#include <entt/entt.hpp>
#include <raylib.h>
#include <random>
#include <array>
#include "game.h"
#include <filesystem>

static auto create_breakout() -> std::unique_ptr<Game> {
    return std::make_unique<BreakoutGame>();
}

bool BreakoutGame::registered = register_game_creator({create_breakout, "Breakout"});

namespace breakout {
    constexpr auto paddleHeight = 15;
    constexpr auto paddleWidth = 70;
    constexpr auto ballSize = 10;
    constexpr auto paddleOffset = ballSize * 2 + 5;
    constexpr auto ballSpeed = 320.0f;
    constexpr auto paddleSpeed = 280.0f;

    constexpr auto blockWidth = 110;
    constexpr auto blockHeight = 20;
    constexpr auto blockGap = 12;
    constexpr auto groupGap = 90;
    constexpr auto groupOffset = 60;
    constexpr auto blockOffset = 50;
    constexpr auto numGroups = 6;

    auto blockHealthColors = std::array{RED, ORANGE, YELLOW, GREEN, VIOLET, PURPLE, BLUE};

    struct Pos {
        float x;
        float y;
    };

    struct RectangleCollision {
        float w;
        float h;
    };

    struct RenderColor {
        Color color;
    };

    struct Controllable {
        int unused;
    };

    struct Velocity {
        float x;
        float y;
    };

    struct CircleCollision {
        float radius;
    };

    struct Paddle {
        int otherScore;
    };

    struct Launchable {
        Pos restart;
    };

    struct Barrier {
        int health;
    };

    struct Score {
        int score;
        int lives;
    };

    struct Random {
        using distribution = std::uniform_real_distribution<float>;
        std::mt19937 engine;
        distribution dst;

        auto operator()() -> float {
            return dst(engine);
        }
    };

    auto register_player(entt::registry &, int y) -> void;

    auto register_ball(entt::registry &) -> void;

    auto register_block(entt::registry &, int x, int y, int width, int height, int health) -> void;

    auto render(const entt::registry &) -> void;

    auto setup_registry() -> entt::registry {
        entt::registry registry;

        // Setup randomness
        std::random_device rd;
        std::mt19937 engine(rd());
        auto entity = registry.create();
        Random::distribution dst(0.0f, 1.0f);
        registry.emplace<Random>(entity, engine, dst);

        for (int group = 0; group < numGroups; ++group) {
            int x = group * (groupGap + blockWidth) + groupOffset;
            for (int block = 0; block < blockHealthColors.size(); ++block) {
                int y = block * (blockHeight + blockGap) + blockOffset;
                register_block(registry, x, y, blockWidth, blockHeight, (blockHealthColors.size() - block));
            }
        }

        register_player(registry, winHeight - paddleHeight - paddleOffset);
        register_ball(registry);
        return registry;
    }

    auto rectangle_circle_collide(const Pos &circlePos, const CircleCollision &circle, const Pos &rectPos,
                                  const RectangleCollision &rectCollision) -> bool {
        float testX = circlePos.x;
        float testY = circlePos.y;

        if (testX < rectPos.x) testX = rectPos.x;
        else if (testX > rectPos.x + rectCollision.w) testX = rectPos.x + rectCollision.w;

        if (testY < rectPos.y) testY = rectPos.y;
        else if (testY > rectPos.y + rectCollision.h) testY = rectPos.y + rectCollision.h;

        float distX = circlePos.x - testX;
        float distY = circlePos.y - testY;
        float distSq = distX * distX + distY * distY;
        float radSq = circle.radius * circle.radius;
        return distSq <= radSq;
    }

    auto register_ball(entt::registry &registry) -> void {
        auto entity = registry.create();
        auto x = static_cast<float>(winWidth) / 2.0f;
        auto y = static_cast<float>(winHeight - ballSize * 3 - paddleOffset);
        registry.emplace<Pos>(entity, x, y);
        registry.emplace<CircleCollision>(entity, static_cast<float>(ballSize));
        registry.emplace<Launchable>(entity, Pos{x, y});
        registry.emplace<RenderColor>(entity, PURPLE);
        registry.emplace<Velocity>(entity, 0.0f, 0.0f);
    }

    auto register_block(entt::registry &registry, int x, int y, int width, int height, int health) -> void {
        auto entity = registry.create();
        registry.emplace<Pos>(entity, static_cast<float>(x), static_cast<float>(y));
        registry.emplace<RectangleCollision>(entity, static_cast<float>(width), static_cast<float>(height));
        registry.emplace<Barrier>(entity, health);
        registry.emplace<RenderColor>(entity, blockHealthColors[health - 1]);
    }

    auto register_player(entt::registry & registry, int y) -> void {
        auto entity = registry.create();
        registry.emplace<Pos>(entity, static_cast<float>(winWidth - paddleWidth) / 2.0f, static_cast<float>(y));
        registry.emplace<RectangleCollision>(entity, static_cast<float>(paddleWidth), static_cast<float>(paddleHeight));
        registry.emplace<Velocity>(entity, 0.0f, 0.0f);
        registry.emplace<Controllable>(entity, 0);
        registry.emplace<RenderColor>(entity, MAGENTA);
        registry.emplace<Paddle>(entity);
        registry.emplace<Score>(entity, 0, 5);
    }

    auto render(const entt::registry &registry) -> void {
        const auto score = registry.get<Score>(registry.view<Score>().front());
        auto scoreText = "Score: " + std::to_string(score.score);
        DrawText(scoreText.c_str(), 1, 1, 25, WHITE);


        auto livesText = "Lives: " + std::to_string(score.lives);
        DrawText(livesText.c_str(), winWidth - 100, 1, 25, WHITE);

        registry.view<const Pos, const RectangleCollision, const RenderColor>().each(
                [](const auto &pos, const auto &rectangleCollision, const RenderColor &rc) {
                    DrawRectangle(
                            static_cast<int>(pos.x),
                            static_cast<int>(pos.y),
                            static_cast<int>(rectangleCollision.w),
                            static_cast<int>(rectangleCollision.h),
                            rc.color
                    );
                });

        registry.view<const Pos, const CircleCollision, const RenderColor>().each(
                [](const auto &pos, const auto &circle, const RenderColor &rc) {
                    DrawCircle(
                            static_cast<int>(pos.x),
                            static_cast<int>(pos.y),
                            static_cast<int>(circle.radius),
                            rc.color
                    );
                });

        const auto barrierViews = registry.view<Pos, const RectangleCollision, const Barrier>();
        if (!score.lives) {
            auto fontSize = 96;
            auto str = "Game Over";
            auto measurements = MeasureText(str, fontSize);
            DrawText(str, winWidth / 2 - measurements / 4, winHeight / 2, 25, WHITE);
        } else if (barrierViews.begin() == barrierViews.end()) {
            auto fontSize = 96;
            auto str = "You Win!";
            auto measurements = MeasureText(str, fontSize);
            DrawText(str, winWidth / 2 - measurements / 4, winHeight / 2, 25, WHITE);
        }
    }
}

BreakoutGame::BreakoutGame() : registry(breakout::setup_registry()) {
    const auto assetPath = std::filesystem::path{"assets"};

    auto launchSoundFile = assetPath / "breakout" / "launch.wav";
    launchSound = LoadSound(launchSoundFile.c_str());
    auto blockBreakSoundFile = assetPath / "breakout" / "break-block.wav";
    blockBreakSound = LoadSound(blockBreakSoundFile.c_str());
    auto blockBounceSoundFile = assetPath / "breakout" / "bounce-block.wav";
    blockBounceSound = LoadSound(blockBounceSoundFile.c_str());
    auto ballDieSoundFile = assetPath / "breakout" / "ball-die.wav";
    ballDieSound = LoadSound(ballDieSoundFile.c_str());
    auto paddleBounceSoundFile = assetPath / "breakout" / "bounce-paddle.wav";
    paddleBounceSound = LoadSound(paddleBounceSoundFile.c_str());
    auto gameLostSoundFile = assetPath / "breakout" / "game-loss.wav";
    gameLostSound = LoadSound(gameLostSoundFile.c_str());
    auto gameWonSoundFile = assetPath / "breakout" / "game-won.wav";
    gameWonSound = LoadSound(gameWonSoundFile.c_str());
    auto musicWave = assetPath / "breakout" / "song.wav";
    bgMusic = LoadMusicStream(musicWave.c_str());
    PlayMusicStream(bgMusic);
    SetMusicVolume(bgMusic, 0.6f);

    inputHandler.register_axis_input_for(InputEvents::MOVE, {
       input::AxisInputDualButtons{
           input::KeyboardInput{KEY_D, input::ButtonRead::DOWN},
           input::KeyboardInput{KEY_A, input::ButtonRead::DOWN},
       },
       input::AxisInputDualButtons{
               input::KeyboardInput{KEY_RIGHT, input::ButtonRead::DOWN},
               input::KeyboardInput{KEY_LEFT, input::ButtonRead::DOWN},
       },
       input::AxisInputDualButtons{
               input::ControllerButtonInput{0, GAMEPAD_BUTTON_LEFT_FACE_LEFT, input::ButtonRead::DOWN},
               input::ControllerButtonInput{0, GAMEPAD_BUTTON_LEFT_FACE_RIGHT, input::ButtonRead::DOWN},
       },
       input::ControllerAxisInput{0, GAMEPAD_AXIS_LEFT_X, false}
    });

    inputHandler.register_button_input_for(InputEvents::LAUNCH, {
            input::KeyboardInput{KEY_SPACE, input::ButtonRead::PRESSED},
            input::ControllerButtonInput{0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN, input::ButtonRead::PRESSED},
    });

    inputHandler.register_button_input_for(InputEvents::RESET, {
            input::KeyboardInput{KEY_R, input::ButtonRead::PRESSED},
            input::ControllerButtonInput{0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN, input::ButtonRead::PRESSED},
    });
}

void BreakoutGame::update() {
    using namespace breakout;

    auto delta = GetFrameTime();
    UpdateMusicStream(bgMusic);

    auto &score = registry.get<Score>(registry.view<Score>().front());
    const auto barrierViews = registry.view<Pos, const RectangleCollision, const Barrier>();

    if (score.lives <= 0 || barrierViews.begin() == barrierViews.end()) {
        if (inputHandler.button_value_of(InputEvents::RESET)) {
            registry = setup_registry();
        }
    }
    else {
        bool launch = false;
        registry.view<Velocity, const Controllable>().each(
                [&](const auto entity, auto &velocity, const Controllable &c) {
                    velocity.x = inputHandler.value_of(InputEvents::MOVE) * paddleSpeed;
                    launch = inputHandler.button_value_of(InputEvents::LAUNCH);
                });

        registry.view<Pos, Velocity, const Launchable>().each(
                [&, launch](const auto entity, auto &pos, auto &velocity, const Launchable &l) {
                    if (abs(velocity.x) > std::numeric_limits<float>::epsilon() ||
                        abs(velocity.y) > std::numeric_limits<float>::epsilon())
                        return;

                    auto paddle = registry.view<Controllable>().front();
                    auto xOffset = registry.get<RectangleCollision>(paddle).w / 2;
                    auto x = registry.get<Pos>(paddle).x;
                    pos.x = x + xOffset;
                    pos.y = l.restart.y;

                    auto randomEntity = registry.view<Random>().front();
                    auto &random = registry.get<Random>(randomEntity);
                    if (launch) {
                        auto angle = random() * PI / 2.0f + 5.0f * PI / 4.0f;
                        velocity.x = ballSpeed * cos(angle);
                        velocity.y = ballSpeed * sin(angle);
                        PlaySound(launchSound);
                    }
                });

        registry.view<Pos, Velocity, const RectangleCollision>().each(
                [delta](const auto entity, auto &pos, auto &vel, auto &rectangleCollision) {
                    pos.x = std::max(0.0f, std::min(static_cast<float>(winWidth) - rectangleCollision.w,
                                                    pos.x + vel.x * delta));
                    pos.y = std::max(0.0f, std::min(static_cast<float>(winHeight) - rectangleCollision.h,
                                                    pos.y + vel.y * delta));
                });

        registry.view<Pos, Velocity, const CircleCollision>().each(
                [&, delta](const auto entity, auto &pos, auto &vel,
                           const CircleCollision &circleCollision) {
                    pos.x += vel.x * delta;
                    pos.y += vel.y * delta;

                    const auto radiusHalf = circleCollision.radius / 2;
                    const auto leftPlane = pos.x - radiusHalf;
                    const auto rightPlane = pos.x + radiusHalf;
                    const auto topPlane = pos.y - radiusHalf;
                    const auto bottomPlane = pos.y + radiusHalf;

                    if (leftPlane <= 0) {
                        vel.x = abs(vel.x);
                    } else if (rightPlane >= winWidth) {
                        vel.x = -abs(vel.x);
                    }
                    if (topPlane <= 0) {
                        vel.y = abs(vel.y);
                    } else if (bottomPlane >= winHeight) {
                        score.lives--;
                        vel.x = 0.0f;
                        vel.y = 0.0f;
                        if (score.lives <= 0) {
                            PlaySound(gameLostSound);
                        } else {
                            PlaySound(ballDieSound);
                        }
                    }

                    std::vector<entt::entity> remove{};
                    auto v = registry.view<Pos, const RectangleCollision, const Barrier>();
                    auto numBarriers = std::distance(v.begin(), v.end());
                    registry.view<Pos, const RectangleCollision>()
                            .each([&, leftPlane, topPlane, bottomPlane](
                                    const auto rectEntity, auto &rectPos, const RectangleCollision &rectColl) {
                                if (entity == rectEntity) return;

                                if (!rectangle_circle_collide(pos, circleCollision, rectPos, rectColl)) {
                                    return;
                                }

                                auto rectMidX = rectPos.x + rectColl.w / 2.0f;
                                auto rectMidY = rectPos.y + rectColl.h / 2.0f;
                                auto rectBottomY = rectPos.y + rectColl.h;

                                if (registry.try_get<Paddle>(rectEntity)) {
                                    auto xDiff = (pos.x - rectMidX);
                                    auto yDiff = pos.y - rectBottomY;
                                    auto h = sqrt(xDiff * xDiff + yDiff * yDiff);
                                    auto angle = sin(yDiff / h);
                                    if (xDiff == 0) {
                                        angle = PI / 2.0f;
                                    } else if (xDiff < 0) {
                                        angle -= PI / 2.0f;
                                    }
                                    vel.y = sin(angle) * ballSpeed;
                                    vel.x = cos(angle) * ballSpeed;
                                    PlaySound(paddleBounceSound);
                                    return;
                                }

                                if (topPlane > abs(rectMidY - bottomPlane) / 2 + rectMidY) {
                                    vel.y = abs(vel.y);
                                } else if (bottomPlane < abs(topPlane - rectMidY) / 2 + rectMidY) {
                                    vel.y = -abs(vel.y);
                                }

                                if (leftPlane > rectMidX) {
                                    vel.x = abs(vel.x);
                                } else {
                                    vel.x = -abs(vel.x);
                                }

                                auto barrier = registry.try_get<Barrier>(rectEntity);
                                if (barrier != nullptr) {
                                    barrier->health--;
                                    score.score += 100;
                                    if (barrier->health <= 0) {
                                        PlaySound(blockBreakSound);
                                        score.score += 50;
                                        remove.emplace_back(rectEntity);
                                    } else {
                                        PlaySound(blockBounceSound);
                                        registry.emplace_or_replace<RenderColor>(rectEntity,
                                                                                 blockHealthColors[barrier->health -
                                                                                                   1]);
                                    }
                                }
                            });

                    if (!remove.empty() && remove.size() == numBarriers) {
                        PlaySound(gameWonSound);
                    }
                    registry.destroy(remove.begin(), remove.end());
                });
    }

    BeginDrawing();
    ClearBackground(BLACK);

    breakout::render(registry);

    EndDrawing();
}

BreakoutGame::~BreakoutGame() {
    StopMusicStream(bgMusic);
    StopSound(launchSound);
    StopSound(blockBreakSound);
    StopSound(blockBounceSound);
    StopSound(paddleBounceSound);
    StopSound(ballDieSound);
    StopSound(gameLostSound);
    StopSound(gameWonSound);

    UnloadSound(launchSound);
    UnloadSound(blockBreakSound);
    UnloadSound(blockBounceSound);
    UnloadSound(paddleBounceSound);
    UnloadSound(ballDieSound);
    UnloadSound(gameLostSound);
    UnloadSound(gameWonSound);
    UnloadMusicStream(bgMusic);
}
