cmake_minimum_required(VERSION 3.18)
project(retro)

set(CMAKE_CXX_STANDARD 20)

add_subdirectory(deps/raylib-4.2.0)
add_subdirectory(deps/entt-3.11.1)

set(GameSources "")
add_custom_target(copy_assets
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/assets ${CMAKE_CURRENT_BINARY_DIR}/assets
        )

macro(add_game GAME)
    list(APPEND GameSources "${GAME}/game.cpp")
    add_executable(${GAME} common/exec.cpp "${GAME}/game.cpp")
    target_link_libraries(${GAME} PRIVATE raylib EnTT)
    add_dependencies(${GAME} copy_assets)
endmacro()

add_game(frogger)
add_game(tictactoe)
add_game(breakout)
add_game(checkers)
add_game(snake)
add_game(pong)

set(BUNDLE_NAME game_collection)
add_executable(${BUNDLE_NAME} common/exec.cpp collection/collection.cpp ${GameSources})
target_link_libraries(${BUNDLE_NAME} PRIVATE  raylib EnTT)
target_compile_definitions(${BUNDLE_NAME} PRIVATE -DEXEC_CLASS_IMPORT="../collection/collection.h" -DEXEC_CLASS_OVERRIDE=Collection)
add_dependencies(${BUNDLE_NAME} copy_assets)
