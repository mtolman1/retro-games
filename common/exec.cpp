#include <raylib.h>
#include "common.h"
#include <cstring>

static std::vector<GameCreator> game_creators = {};

bool register_game_creator(GameCreator creator) {
    game_creators.emplace_back(std::move(creator));
    return true;
}

auto get_game_creators() -> const std::vector<GameCreator>& {
    return game_creators;
}

#ifdef EXEC_CLASS_IMPORT
#include EXEC_CLASS_IMPORT
#endif

int main(int argc, const char* const * argv) {
#ifdef EXEC_CLASS_OVERRIDE
    InitWindow(winWidth, winHeight, "Game Collection");
    InitAudioDevice();

    if (argc <= 1 || strcmp(argv[1], "windowed") != 0) {
        ToggleFullscreen();
    }

    auto game = std::make_unique<EXEC_CLASS_OVERRIDE>();
#else
    auto creator = get_game_creators()[0];
    InitWindow(winWidth, winHeight, creator.name);
    InitAudioDevice();
    auto game = creator.create();
#endif

    while (!WindowShouldClose())
    {
        game->update();
    }

    CloseAudioDevice();
    CloseWindow();
    return 0;
}
