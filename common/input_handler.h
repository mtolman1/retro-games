#pragma once

#include <raylib.h>
#include <variant>

namespace input {
    enum class InputOutputType {
        BUTTON,
        AXIS
    };

    enum class ButtonRead {
        DOWN,
        PRESSED,
        UP,
        RELEASED
    };

    struct KeyboardInput {
        int key;
        ButtonRead read;

        [[nodiscard]] auto value() const -> double {
            switch (read) {
                case ButtonRead::DOWN: return IsKeyDown(key) ? 1.0f : 0.0f;
                case ButtonRead::UP: return IsKeyUp(key) ? 1.0f : 0.0f;
                case ButtonRead::PRESSED: return IsKeyPressed(key) ? 1.0f : 0.0f;
                case ButtonRead::RELEASED: return IsKeyReleased(key) ? 1.0f : 0.0f;
                default: return 0.0f;
            }
        }
    };

    struct ControllerButtonInput {
        int controller;
        int button;
        ButtonRead read;

        [[nodiscard]] auto value() const -> double {
            switch (read) {
                case ButtonRead::DOWN: return IsGamepadButtonDown(controller, button) ? 1.0f : 0.0f;
                case ButtonRead::UP: return IsGamepadButtonUp(controller, button) ? 1.0f : 0.0f;
                case ButtonRead::PRESSED: return IsGamepadButtonPressed(controller, button) ? 1.0f : 0.0f;
                case ButtonRead::RELEASED: return IsGamepadButtonReleased(controller, button) ? 1.0f : 0.0f;
                default: return 0.0f;
            }
        }
    };

    using ButtonInput = std::variant<KeyboardInput, ControllerButtonInput>;

    struct AxisInputDualButtons {
        ButtonInput positive;
        ButtonInput negative;

        [[nodiscard]] auto value() const -> double {
            auto value = 0.0;
            value += std::visit([](const auto& input){ return input.value(); }, positive);
            value -= std::visit([](const auto& input){ return input.value(); }, negative);
            return value;
        }
    };

    struct ControllerAxisInput {
        int controller;
        int axis;
        bool invert;

        [[nodiscard]] auto value() const -> double { return GetGamepadAxisMovement(controller, axis) * (invert ? -1.0 : 1.0); }
    };

    using AxisInput = std::variant<AxisInputDualButtons, ControllerAxisInput>;

    template<typename Event>
    class InputHandler {
    public:
        auto register_axis_input_for(Event event, std::vector<AxisInput> inputs) -> void { axisInputs[event] = std::move(inputs); }
        auto register_button_input_for(Event event, const std::vector<ButtonInput>& inputs) -> void {
            std::vector<AxisInput> axes{};
            axes.reserve(inputs.size());
            std::transform(inputs.begin(), inputs.end(), std::back_inserter(axes), [](const ButtonInput& buttonInput) {
                return AxisInputDualButtons{buttonInput, KeyboardInput{0, ButtonRead::RELEASED}};
            });
            axisInputs[event] = std::move(axes);
        }

        auto value_of(Event event) -> double {
            if (!axisInputs.contains(event)) {
                return 0.0;
            }
            const auto& rawInputs = axisInputs.at(event);
            std::vector<double> inputs{};
            inputs.resize(rawInputs.size());
            std::transform(rawInputs.begin(), rawInputs.end(), inputs.begin(), [](const AxisInput& i) { return std::visit([](const auto& input){ return input.value(); }, i); });
            double res = 0.0f;
            for (double val : inputs) {
                if (abs(val) > abs(res)) {
                    res = val;
                }
            }
            return res;
        }

        auto button_value_of(Event event) -> bool {
            return abs(value_of(event)) > 0.5;
        }
    private:
        std::map<Event, std::vector<AxisInput>, std::less<>> axisInputs{};
    };
}
