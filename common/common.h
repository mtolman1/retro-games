#pragma once

#include <vector>
#include <memory>
#include <functional>

class Game {
public:
    virtual ~Game() = default;
    virtual void update() = 0;
};

constexpr auto winHeight = 720;
constexpr auto winWidth = 1280;

struct GameCreator {
    std::function<std::unique_ptr<Game>(void)> create;
    const char* name;
};

extern auto register_game_creator(GameCreator) -> bool;
extern auto get_game_creators() -> const std::vector<GameCreator>&;
