#include "../common/common.h"
#include <array>
#include <raylib.h>
#include <cmath>
#include <filesystem>
#include <optional>

namespace tictactoe {

    constexpr auto size = winHeight;
    constexpr auto cellSize = size / 3.0f;
    constexpr auto xOffsetLeft = winWidth / 2 - size / 2;
    constexpr auto xLine1 = xOffsetLeft + cellSize;
    constexpr auto xLine2 = xOffsetLeft + cellSize * 2;
    constexpr auto thickness = 15;
    constexpr auto large_radius = (cellSize - 60) / 2;
    constexpr auto small_radius = (cellSize - 60) / 4;
    constexpr auto move_cool_down = 2;

    enum class GameState {
        PLAYING,
        X_WINS,
        O_WINS,
        DRAW
    };
    
    enum class Cell {
        EMPTY,
        X,
        O
    };
    
    class TicTacToe : public Game {
    public:
        TicTacToe();
        ~TicTacToe() override;
        void update() override {
            UpdateMusicStream(bgMusic);
            ++framesSinceMove;
            update_game();
            render();
        }

        using Board = std::array<std::array<Cell, 3>, 3>;
    private:
        auto render() -> void;
        auto update_game() -> void;
        
        Board board = {};

        [[nodiscard]] auto gamepad_for_cur_player() const -> int {
            return isXTurn ? 0 : 1;
        }
        
        bool isXTurn = true;

        int cursorXPos = 0;
        int cursorYPos = 0;

        GameState gameState = GameState::PLAYING;

        [[nodiscard]] auto player_did_input_move_left() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }

            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_LEFT_FACE_LEFT)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT);
        }

        [[nodiscard]] auto player_did_input_move_right() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }
            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_LEFT_FACE_RIGHT)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT);
        }

        [[nodiscard]] auto player_did_input_move_down() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }

            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_LEFT_FACE_DOWN)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN);
        }

        [[nodiscard]] auto player_did_input_move_up() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }

            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_LEFT_FACE_UP)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP);
        }

        [[nodiscard]] auto player_did_input_game_reset() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }
            return IsKeyPressed(KEY_R) || IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN) || IsGamepadButtonPressed(1, GAMEPAD_BUTTON_RIGHT_FACE_DOWN);
        }

        [[nodiscard]] auto player_did_input_make_move() const -> bool {
            if (framesSinceMove <= move_cool_down) {
                return false;
            }
            if (IsGamepadAvailable(gamepad_for_cur_player())) {
                if (IsGamepadButtonPressed(gamepad_for_cur_player(), GAMEPAD_BUTTON_RIGHT_FACE_DOWN)) {
                    return true;
                }
            }
            return IsKeyPressed(KEY_SPACE) || IsKeyPressed(KEY_ENTER);
        }
        
        auto check_for_game_end() -> void;
        
        [[nodiscard]] auto is_board_full() const -> bool;

        static bool registered;
        int framesSinceMove = 0;
        
        Sound draw;
        Sound move;
        Sound invalid;
        Sound selectEmpty;
        Sound selectTaken;
        Sound victory;
        Music bgMusic;
    };

    TicTacToe::TicTacToe() {
        const auto assetPath = std::filesystem::path{"assets"} / "tic-tac-toe";
        
        auto drawFile = assetPath / "draw.wav";
        draw = LoadSound(drawFile.c_str());
        auto moveFile = assetPath / "move.wav";
        move = LoadSound(moveFile.c_str());
        auto invalidFile = assetPath / "invalid-move.wav";
        invalid = LoadSound(invalidFile.c_str());
        auto selectEmptyFile = assetPath / "select-good.wav";
        selectEmpty = LoadSound(selectEmptyFile.c_str());
        auto selectTakenFile = assetPath / "select-taken.wav";
        selectTaken = LoadSound(selectTakenFile.c_str());
        auto victoryFile = assetPath / "victory.wav";
        victory = LoadSound(victoryFile.c_str());
        auto bgMusicFile = assetPath / "song.wav";
        bgMusic = LoadMusicStream(bgMusicFile.c_str());

        PlayMusicStream(bgMusic);
        SetMusicVolume(bgMusic, 0.35f);
    }

    TicTacToe::~TicTacToe() {
        StopSound(draw);
        StopSound(move);
        StopSound(invalid);
        StopSound(selectEmpty);
        StopSound(selectTaken);
        StopSound(victory);
        StopMusicStream(bgMusic);

        UnloadSound(draw);
        UnloadSound(move);
        UnloadSound(invalid);
        UnloadSound(selectEmpty);
        UnloadSound(selectTaken);
        UnloadSound(victory);
        UnloadMusicStream(bgMusic);
    }

    auto draw_x(int x, int y, float radius, Color color) {
        auto adj = radius * std::sqrt(2.0f) / 2.0f;
        DrawLineEx(Vector2{x - adj, y - adj}, Vector2{x + adj, y + adj}, thickness, RED);
        DrawLineEx(Vector2{x - adj, y + adj}, Vector2{x + adj, y - adj}, thickness, RED);
    }

    auto draw_o(int x, int y, float radius, Color color) {
        DrawCircle(x, y, radius, BLUE);
        DrawCircle(x, y, radius - thickness, RAYWHITE);
    }

    auto draw_highlight(int row, int col, Color color) {
        auto x = xOffsetLeft + cellSize * col;
        auto y = cellSize * row;
        DrawRectangleLinesEx(Rectangle{x, y, cellSize, cellSize}, thickness / 2, color);
    }

    auto TicTacToe::render() -> void {
        BeginDrawing();

        ClearBackground(RAYWHITE);

        // vertical lines
        DrawLineEx(
                Vector2{xLine1, 0},
                Vector2{xLine1, winHeight},
                5,
                BLACK
        );

        DrawLineEx(
                Vector2{xLine2, 0},
                Vector2{xLine2, winHeight},
                5,
                BLACK
        );

        // horizontal lines
        DrawLineEx(
                Vector2{xOffsetLeft, cellSize},
                Vector2{xOffsetLeft + size, cellSize},
                5,
                BLACK
        );

        DrawLineEx(
                Vector2{static_cast<float>(xOffsetLeft), size * 2.0f / 3.0f},
                Vector2{static_cast<float>(xOffsetLeft + size), size * 2.0f / 3.0f},
                5,
                BLACK
        );

        draw_highlight(cursorYPos, cursorXPos, board[cursorYPos][cursorXPos] == Cell::EMPTY ? YELLOW : DARKGRAY);
        
        for (auto row = 0; row < board.size(); ++row) {
            for (auto col = 0; col < board[row].size(); ++col) {
                const auto& cell = board[row][col];
                auto x = xOffsetLeft + cellSize * col + cellSize / 2;
                auto y = cellSize * row + cellSize / 2;
                if (cell == Cell::X) {
                    draw_x(x, y, large_radius, RED);
                }
                else if (cell == Cell::O) {
                    draw_o(x, y, large_radius, BLUE);
                }
            }
        }

        if (gameState == GameState::PLAYING) {
            if (isXTurn) {
                draw_x(small_radius + 5, small_radius + 5, small_radius, RED);
            } else {
                draw_o(winWidth - small_radius - 5, small_radius + 5, small_radius, BLUE);
            }
        }
        else if (gameState == GameState::DRAW) {
            auto x = xOffsetLeft + cellSize * 1 - 40;
            auto y = cellSize * 1;
            DrawRectangle(x, y, cellSize + 80, cellSize, WHITE);
            DrawText("Draw", winWidth/2 - 110, winHeight/2 - 46, 96, BLACK);
        }
        else if (gameState == GameState::X_WINS) {
            auto x = xOffsetLeft + cellSize * 1 - 80;
            auto y = cellSize * 1;
            DrawRectangle(x, y, cellSize + 160, cellSize, WHITE);
            DrawText("X Wins!", winWidth/2 - 160, winHeight/2 - 46, 96, BLACK);
        }
        else if (gameState == GameState::O_WINS) {
            auto x = xOffsetLeft + cellSize * 1 - 80;
            auto y = cellSize * 1;
            DrawRectangle(x, y, cellSize + 160, cellSize, WHITE);
            DrawText("O Wins!", winWidth/2 - 160, winHeight/2 - 46, 96, BLACK);
        }
        
        EndDrawing();
    }

    auto TicTacToe::update_game() -> void {
        if (gameState != GameState::PLAYING) {
            if (player_did_input_game_reset()) {
                board = {};
                gameState = GameState::PLAYING;
                cursorXPos = 0;
                cursorYPos = 0;
                isXTurn = true;
                PlaySound(move);
            }
            return;
        }

        bool moved = false;
        if (player_did_input_move_left()) {
            --cursorXPos;
            if (cursorXPos < 0) {
                cursorXPos = 2;
            }
            moved = true;
        }
        else if (player_did_input_move_right()) {
            cursorXPos = (cursorXPos + 1) % 3;
            moved = true;
        }
        else if (player_did_input_move_down()) {
            cursorYPos = (cursorYPos + 1) % 3;
            moved = true;
        }
        else if (player_did_input_move_up()) {
            --cursorYPos;
            if (cursorYPos < 0) {
                cursorYPos = 2;
            }
            moved = true;
        }
        else if (player_did_input_make_move()) {
            if (board[cursorYPos][cursorXPos] == Cell::EMPTY) {
                board[cursorYPos][cursorXPos] = isXTurn ? Cell::X : Cell::O;
                isXTurn = !isXTurn;
                PlaySound(move);
            }
            else {
                PlaySound(invalid);
            }
        }

        if (moved) {
            PlaySound(board[cursorYPos][cursorXPos] == Cell::EMPTY ? selectEmpty : selectTaken);
        }
        
        check_for_game_end();
    }

    bool TicTacToe::registered = register_game_creator({[]() { return std::make_unique<TicTacToe>(); }, "Tic-Tac-Toe"});

    auto TicTacToe::is_board_full() const -> bool {
        for (const auto& row : board) {
            for (const auto& cell : row) {
                if (cell == Cell::EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    struct CheckPos {
        int row;
        int col;
    };

    auto player_if_same(const std::array<CheckPos, 3>& checks, const TicTacToe::Board& board) -> std::optional<Cell> {
        auto res = board[checks[0].row][checks[0].col];
        if (board[checks[1].row][checks[1].col] == res && board[checks[2].row][checks[2].col] == res) {
            return res;
        }
        return std::nullopt;
    }

    constexpr auto cellComparisons = std::array{
            std::array{CheckPos{0, 0}, CheckPos{0, 1}, CheckPos{0, 2}},
            std::array{CheckPos{1, 0}, CheckPos{1, 1}, CheckPos{1, 2}},
            std::array{CheckPos{2, 0}, CheckPos{2, 1}, CheckPos{2, 2}},

            std::array{CheckPos{0, 0}, CheckPos{1, 0}, CheckPos{2, 0}},
            std::array{CheckPos{0, 1}, CheckPos{1, 1}, CheckPos{2, 1}},
            std::array{CheckPos{0, 2}, CheckPos{1, 2}, CheckPos{2, 2}},

            std::array{CheckPos{0, 0}, CheckPos{1, 1}, CheckPos{2, 2}},
            std::array{CheckPos{0, 2}, CheckPos{1, 1}, CheckPos{2, 0}},
    };
    
    auto TicTacToe::check_for_game_end() -> void {
        for (const auto& comparison : cellComparisons) {
            auto player = player_if_same(comparison, board);
            if (player == Cell::X) {
                gameState = GameState::X_WINS;
                PlaySound(victory);
            }
            else if (player == Cell::O) {
                gameState = GameState::O_WINS;
                PlaySound(victory);
            }
        }

        if (is_board_full()) {
            gameState = GameState::DRAW;
            PlaySound(draw);
        }
    }
}