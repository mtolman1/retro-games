## Prerequisites

Run the following command (Debian-based):

```bash
sudo apt-get install -y build-essential cmake libx11-dev libxrandr-dev libxinerama-dev freeglut3-dev libxcursor-dev libxi-dev
```
