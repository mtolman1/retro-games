#include <entt/entt.hpp>
#include <raylib.h>
#include <random>
#include <filesystem>
#include "../common/common.h"
#include "../common/input_handler.h"

namespace frogger {
    constexpr int spriteSize = 72;
    constexpr

    struct AnimGridPos {
        int prevCellX;
        int prevCellY;
        int targetCellX;
        int targetCellY;
        double moveStartTime = 0.0;
        double animSpeed = 1.0;

        auto finishedAnim() -> bool {
            return (GetTime() - moveStartTime) > animSpeed;
        }
    };

    class Frogger : public Game {
    public:
        Frogger() {
            const auto assetPath = std::filesystem::path{"assets"} / "frogger";

            auto frogTextureFile = assetPath / "frog.png";
            frogTexture = LoadTexture(frogTextureFile.c_str());

            auto logTextureFile = assetPath / "log.png";
            logTexture = LoadTexture(logTextureFile.c_str());
        }

        ~Frogger() override {
            UnloadTexture(frogTexture);
            UnloadTexture(logTexture);
        }

        void update() override {
            update_game();
            render();
        }

    private:
        void update_game();
        void render();

        Texture logTexture;
        Texture frogTexture;

        int frogFrame = 0;

        static bool registered;
    };

    void Frogger::update_game() {
        frogFrame = static_cast<int>(GetTime()) % 6;
    }

    void Frogger::render() {
        BeginDrawing();
        ClearBackground(BLACK);

        DrawTextureRec(frogTexture, Rectangle{static_cast<float>(frogFrame * spriteSize), 0, spriteSize, spriteSize}, Vector2{winWidth / 2.0f, winHeight / 2.0f}, WHITE);

        EndDrawing();
    }
}

bool frogger::Frogger::registered = register_game_creator({[]() { return std::make_unique<Frogger>(); }, "Frogger"});
