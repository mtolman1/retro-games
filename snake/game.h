#pragma once

#include "../common/common.h"
#include "../common/input_handler.h"
#include <entt/entt.hpp>

class SnakeGame : public Game {
public:
    SnakeGame();
    void update() override;
    ~SnakeGame() override;

    enum class InputEvents {
        TURN_NORTH,
        TURN_SOUTH,
        TURN_WEST,
        TURN_EAST,
        RESET,
    };
private:
    entt::registry registry;
    static bool registered;

    Sound appleSound;
    Sound dieSound;
    Music bgMusic;
    input::InputHandler<InputEvents> inputHandler;
};
