#include <entt/entt.hpp>
#include <raylib.h>
#include <random>
#include "game.h"
#include <filesystem>

static auto create_snake() -> std::unique_ptr<Game> {
    return std::make_unique<SnakeGame>();
}

bool SnakeGame::registered = register_game_creator({create_snake, "Snake"});

namespace snake {
    struct GridPos {
        int x;
        int y;
    };

    struct RandomCell {
        using distribution = std::uniform_real_distribution<float>;
        std::mt19937 engine;
        distribution dstX;
        distribution dstY;

        auto x() -> int { return static_cast<int>(round(dstX(engine))); }
        auto y() -> int { return static_cast<int>(round(dstY(engine))); }
        auto operator()() -> GridPos { return {x(), y()}; }
    };

    struct SnakeCell {
        int framesLeft;
    };

    struct IncreaseLength {
        int amount;
    };

    enum class GamePhase {
        GAME,
        LOSS
    };

    struct Score {
        int score;
    };

    struct GameState {
        GamePhase phase;
    };

    struct StateTracker {
        double elapsedTime = 0.0f;
        static constexpr double frameTime = 1.0f/6.0f;

        double startTime = GetTime();

        auto increment() {
            auto newTime = GetTime();
            elapsedTime += newTime - startTime;
            startTime = newTime;
        }

        [[nodiscard]] auto get_frame() const -> bool {
            if (elapsedTime > frameTime) {
                return true;
            }
            return false;
        }

        auto reset_frame_count() {
            elapsedTime = 0.0;
        }
    };

    struct SnakeHead {
        int xVec = 0;
        int yVec = 0;
        int frameLiveness = 1;
    };

    struct Controllable {
        int unused;
    };

    enum class RenderShape {
        SQUARE,
        CIRCLE,
    };

    struct Render {
        Color color;
        RenderShape shape;
    };

    constexpr auto cellSize = 30;
    constexpr auto cellBorder = 1;
    constexpr auto gridMargin = 10;
    constexpr auto header = 60;
    constexpr auto footer = 0;
    constexpr auto gridCellsX = (winWidth - (gridMargin * 2))/ (cellSize + cellBorder);
    constexpr auto gridCellsY = (winHeight - (gridMargin * 2) - header - footer)/ (cellSize + cellBorder);

    auto render(const entt::registry&) -> void;
    auto register_player(entt::registry&, int x, int y, int keyNorth, int keySouth, int keyWest, int keyEast, int gamePad, int gpNorth, int gpSouth, int gpWest, int gpEast) -> void;
    auto update(entt::registry&, float) -> void;

    auto setup_game() -> entt::registry;

    auto setup_game() -> entt::registry {
        entt::registry registry;

        // Setup randomness
        std::random_device rd;
        std::mt19937 engine(rd());
        auto entity = registry.create();
        RandomCell::distribution dstX(0, gridCellsX - 1);
        RandomCell::distribution dstY(0, gridCellsY - 1);
        registry.emplace<RandomCell>(entity, engine, dstX, dstY);

        // Setup snake
        register_player(registry, gridCellsX/2, gridCellsY/2,
                        // Keyboard controls
                        KEY_W, KEY_S, KEY_A, KEY_D,
                        // Gamepad controls
                        0, GAMEPAD_BUTTON_LEFT_FACE_UP, GAMEPAD_BUTTON_LEFT_FACE_DOWN, GAMEPAD_BUTTON_LEFT_FACE_LEFT, GAMEPAD_BUTTON_LEFT_FACE_RIGHT);
        return registry;
    }

    auto render(const entt::registry& registry) -> void {
        BeginDrawing();
        ClearBackground(RAYWHITE);

        auto cell = cellSize + cellBorder;

        DrawRectangleLines(gridMargin - 1, header - 1, gridCellsX * cell + 2, gridCellsY * cell + 2, BLACK);
        DrawRectangleLines(gridMargin, header, gridCellsX * cell, gridCellsY * cell, BLACK);
        DrawRectangleLines(gridMargin + 1, header + 1, gridCellsX * cell - 2, gridCellsY * cell - 2, BLACK);

        registry.view<const GridPos, const Render>()
                .each([cell](const GridPos& gridPos, const Render& render) {
            if (render.shape == RenderShape::SQUARE) {
                        DrawRectangle(
                                gridPos.x * cell + gridMargin,
                                gridPos.y * cell + header,
                                cellSize - cellBorder,
                                cellSize - cellBorder,
                                render.color
                        );
            }
            else {
                DrawCircle(
                        gridPos.x * cell + cell/2 + gridMargin + cellBorder,
                        gridPos.y * cell + cell/2 + header + cellBorder,
                        (cellSize - cellBorder) / 2,
                        render.color
                );
            }
                });

        registry.view<const Score>().each([](const Score& s) {
            auto str = "Score: " + std::to_string(s.score);
            DrawText(str.c_str(), 1, 1, 25, BLACK);
        });

        registry.view<const GameState>().each([](const GameState& gm) {
            if (gm.phase == GamePhase::LOSS) {
                if (!IsGamepadAvailable(0)) {
                    DrawText("Game Over. Press 'R' to restart.", 1, 30, 25, BLACK);
                }
                else {
                    DrawText("Game Over. Press 'A' to restart.", 1, 30, 25, BLACK);
                }
            }
        });

        EndDrawing();
    }

    auto register_player(entt::registry& registry, int x, int y, int keyNorth, int keySouth, int keyWest, int keyEast, int gamePad, int gpNorth, int gpSouth, int gpWest, int gpEast) -> void {
        auto entity = registry.create();
        registry.emplace<GridPos>(entity, x, y);
        registry.emplace<Render>(entity, DARKGREEN, RenderShape::SQUARE);
        registry.emplace<Controllable>(entity, 0);
        registry.emplace<SnakeHead>(entity, 0, 0, 2);
        registry.emplace<Score>(entity, 0);
        registry.emplace<GameState>(entity, GamePhase::GAME);
        registry.emplace<StateTracker>(entity, 0.0f);
    }
}

SnakeGame::SnakeGame() : registry(snake::setup_game()) {
    const auto assetPath = std::filesystem::path{"assets"};

    auto applSoundFile = assetPath / "snake" / "apple.wav";
    appleSound = LoadSound(applSoundFile.c_str());
    auto dieSoundFile = assetPath / "snake" / "die.wav";
    dieSound = LoadSound(dieSoundFile.c_str());
    auto musicWave = assetPath / "snake" / "song.wav";
    bgMusic = LoadMusicStream(musicWave.c_str());
    PlayMusicStream(bgMusic);
    SetMusicVolume(bgMusic, 0.6f);

    inputHandler.register_button_input_for(InputEvents::TURN_NORTH, {
            input::KeyboardInput{KEY_W, input::ButtonRead::PRESSED},
            input::KeyboardInput{KEY_UP, input::ButtonRead::PRESSED},
            input::ControllerButtonInput{0, GAMEPAD_BUTTON_LEFT_FACE_UP, input::ButtonRead::PRESSED}
    });

    inputHandler.register_button_input_for(InputEvents::TURN_SOUTH, {
            input::KeyboardInput{KEY_S, input::ButtonRead::PRESSED},
            input::KeyboardInput{KEY_DOWN, input::ButtonRead::PRESSED},
            input::ControllerButtonInput{0, GAMEPAD_BUTTON_LEFT_FACE_DOWN, input::ButtonRead::PRESSED}
    });

    inputHandler.register_button_input_for(InputEvents::TURN_WEST, {
            input::KeyboardInput{KEY_A, input::ButtonRead::PRESSED},
            input::KeyboardInput{KEY_LEFT, input::ButtonRead::PRESSED},
            input::ControllerButtonInput{0, GAMEPAD_BUTTON_LEFT_FACE_LEFT, input::ButtonRead::PRESSED}
    });

    inputHandler.register_button_input_for(InputEvents::TURN_EAST, {
            input::KeyboardInput{KEY_D, input::ButtonRead::PRESSED},
            input::KeyboardInput{KEY_RIGHT, input::ButtonRead::PRESSED},
            input::ControllerButtonInput{0, GAMEPAD_BUTTON_LEFT_FACE_RIGHT, input::ButtonRead::PRESSED}
    });

    inputHandler.register_button_input_for(InputEvents::RESET, {
            input::KeyboardInput{KEY_R, input::ButtonRead::PRESSED},
            input::ControllerButtonInput{0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN, input::ButtonRead::PRESSED}
    });
}

void SnakeGame::update() {
    using namespace snake;

    UpdateMusicStream(bgMusic);

    if (inputHandler.button_value_of(InputEvents::RESET) &&
        registry.get<GameState>(registry.view<GameState>().front()).phase == GamePhase::LOSS) {
        registry = setup_game();
    }
    else {
        bool playerInputedValidMove = false;
        registry.view<SnakeHead, const Controllable>()
                .each([&](SnakeHead &snakeHead, const Controllable &controls) {
                    auto oldXDir = snakeHead.xVec;
                    auto oldYDir = snakeHead.yVec;

                    snakeHead.xVec = 0;
                    snakeHead.yVec = 0;
                    if (inputHandler.button_value_of(InputEvents::TURN_NORTH)) {
                        snakeHead.yVec--;
                    }
                    if (inputHandler.button_value_of(InputEvents::TURN_SOUTH)) {
                        snakeHead.yVec++;
                    }
                    if (inputHandler.button_value_of(InputEvents::TURN_WEST)) {
                        snakeHead.xVec--;
                    }
                    if (inputHandler.button_value_of(InputEvents::TURN_EAST)) {
                        snakeHead.xVec++;
                    }

                    // Normalize for a directional input
                    if ((!snakeHead.xVec && !snakeHead.yVec)) {
                        snakeHead.xVec = oldXDir;
                        snakeHead.yVec = oldYDir;
                    } else if (snakeHead.xVec && snakeHead.yVec) {
                        if (oldXDir) {
                            snakeHead.yVec = snakeHead.yVec;
                            snakeHead.xVec = 0;
                        } else {
                            snakeHead.xVec = snakeHead.xVec;
                            snakeHead.yVec = 0;
                        }
                    }

                    // Prevent 180's
                    if ((oldXDir && -oldXDir == snakeHead.xVec) || (oldYDir && -oldYDir == snakeHead.yVec)) {
                        snakeHead.xVec = oldXDir;
                        snakeHead.yVec = oldYDir;
                    } else {
                        playerInputedValidMove = snakeHead.xVec != oldXDir || snakeHead.yVec != oldYDir;
                    }
                });

        {
            auto &rand = registry.get<RandomCell>(registry.view<RandomCell>().front());

            auto appleView = registry.view<IncreaseLength, GridPos>();
            if (appleView.begin() == appleView.end()) {
                // apply placement trials per frame
                for (size_t i = 0; i < 30; ++i) {
                    bool found = false;
                    auto pos = GridPos{rand.x(), rand.y()};
                    for (auto [entity, takenPos]: registry.view<GridPos>().each()) {
                        if (takenPos.x == pos.x && takenPos.y == pos.y) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        auto apple = registry.create();
                        registry.emplace<GridPos>(apple, pos.x, pos.y);
                        registry.emplace<IncreaseLength>(apple, 1);
                        registry.emplace<Render>(apple, RED, RenderShape::CIRCLE);
                        break;
                    }
                }
            }
        }

        auto &shouldUpdateState = registry.get<StateTracker>(registry.view<StateTracker>().front());
        shouldUpdateState.increment();

        if (shouldUpdateState.get_frame() || playerInputedValidMove) {
            shouldUpdateState.reset_frame_count();
            auto snakeHeadView = registry.view<SnakeHead, GridPos, const Render>();
            if (snakeHeadView.begin() == snakeHeadView.end()) {
                registry.view<GameState>().each([](GameState &gm) {
                    gm.phase = GamePhase::LOSS;
                });
            }

            std::vector<entt::entity> toDestroy = {};
            snakeHeadView
                    .each([&](const auto entity, SnakeHead &snakeHead, GridPos &gridPos,
                                                  const Render &render) {
                        auto newCell = registry.create();
                        registry.emplace<SnakeCell>(newCell, snakeHead.frameLiveness + 1);
                        registry.emplace<GridPos>(newCell, gridPos.x, gridPos.y);
                        auto cellColor = ColorToHSV(render.color);
                        cellColor.z *= 1.5;
                        registry.emplace<Render>(newCell, ColorFromHSV(cellColor.x, cellColor.y, cellColor.z),
                                                 RenderShape::SQUARE);

                        gridPos.x += snakeHead.xVec;
                        gridPos.y += snakeHead.yVec;

                        if (gridPos.x < 0 || gridPos.x >= gridCellsX
                            || gridPos.y < 0 || gridPos.y >= gridCellsY) {
                            registry.remove<SnakeHead>(entity);
                            registry.remove<Render>(entity);
                            PlaySound(dieSound);
                        }
                    });

            int decrement = 1;
            registry.view<const IncreaseLength, const GridPos>()
                    .each([&](const auto appleEntity, const IncreaseLength &il,
                                                              const auto &pos) {
                        registry.view<SnakeHead, Score, const GridPos>()
                                .each([&](SnakeHead &snakeHead,
                                                                                        Score &score,
                                                                                        const GridPos &snakePos) {
                                if (snakePos.x != pos.x || snakePos.y != pos.y) return;
                                    decrement = 0;
                                    snakeHead.frameLiveness += il.amount;
                                    toDestroy.emplace_back(appleEntity);
                                    score.score++;
                                    PlaySound(appleSound);
                                });
                    });

            registry.view<SnakeCell, const GridPos>().each(
                    [&](const auto entity, SnakeCell &snakeCell,
                                                       const GridPos &cellPos) {
                        snakeCell.framesLeft -= decrement;
                        if (snakeCell.framesLeft <= 0) {
                            toDestroy.emplace_back(entity);
                            return;
                        }
                        registry.view<SnakeHead, const GridPos>()
                                .each([&](const auto headEntity, SnakeHead &snakeHead,
                                                            const GridPos &snakePos) {
                                    if ((snakeHead.yVec != 0 || snakeHead.xVec != 0) && snakePos.x == cellPos.x &&
                                        snakePos.y == cellPos.y) {
                                        registry.remove<SnakeHead>(headEntity);
                                        PlaySound(dieSound);
                                    }
                                });
                    });

            registry.destroy(toDestroy.begin(), toDestroy.end());
        }
    }
    snake::render(registry);
}

SnakeGame::~SnakeGame() {
    StopSound(appleSound);
    StopSound(dieSound);
    StopMusicStream(bgMusic);

    UnloadSound(appleSound);
    UnloadSound(dieSound);
    UnloadMusicStream(bgMusic);
}
