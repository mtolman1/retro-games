#include "collection.h"
#include <filesystem>
#include <iostream>

constexpr auto fontSize = 46;
constexpr auto gap = 25;
constexpr auto regionHeight = 190;
constexpr auto transitionTime = 0.2f;

Collection::Collection() {
    SetExitKey(KEY_DELETE);
    const auto assetPath = std::filesystem::path{"assets"};
    for (const auto& creator : gameOptions) {
        auto assetName = assetPath / (std::string{creator.name} + ".png");
        if (!std::filesystem::exists(assetName)) {
            std::cerr << "Couldn't find file " << std::filesystem::absolute(assetName) << "\n";
        }
        else {
            gameIcons[creator.name] = LoadTexture(assetName.c_str());
        }
    }
    auto selectWave = assetPath / "menu" / "select.wav";
    selectChange = LoadSound(selectWave.c_str());
    auto selectGame = assetPath / "menu" / "enter-game.wav";
    gameSelected = LoadSound(selectGame.c_str());
    auto musicWave = assetPath / "menu" / "song.wav";
    bgMusic = LoadMusicStream(musicWave.c_str());
    PlayMusicStream(bgMusic);
    SetMusicVolume(bgMusic, 0.25f);
}

void Collection::update() {
    timeSinceLastSelectionChange += GetFrameTime();
    if (!currentGame) {
        auto newMenuHover = menuHover;
        if (IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP) || IsGamepadButtonPressed(0, GAMEPAD_BUTTON_LEFT_FACE_UP)) {
            newMenuHover = std::max(0, menuHover - 1);
            PlaySound(selectChange);
        } else if (IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN) || IsGamepadButtonPressed(0, GAMEPAD_BUTTON_LEFT_FACE_DOWN)) {
            newMenuHover = std::min<int>(gameOptions.size() - 1, menuHover + 1);
            PlaySound(selectChange);
        } else if (IsKeyPressed(KEY_ENTER) || IsKeyPressed(KEY_SPACE) || IsGamepadButtonPressed(0, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)) {
            StopMusicStream(bgMusic);
            currentGame = gameOptions[menuHover].create();
            PlaySound(gameSelected);
            return;
        }
        if (newMenuHover != menuHover) {
            lastMenuHover = menuHover;
            menuHover = newMenuHover;
            timeSinceLastSelectionChange = 0.0f;
        }
        UpdateMusicStream(bgMusic);

        BeginDrawing();
        ClearBackground(BLACK);

        auto textVOffset = (regionHeight - fontSize) / 2;

        auto prevMenuItemOffset = (lastMenuHover) * -regionHeight;
        auto curMenuItemOffset = (menuHover) * -regionHeight;
        auto deltaTime = std::min(timeSinceLastSelectionChange/transitionTime, 1.0f);
        auto deltaOffset = (curMenuItemOffset - prevMenuItemOffset) * deltaTime + prevMenuItemOffset;
        auto totalOffset = deltaOffset + winHeight/2 - regionHeight/2;

        for (int index = 0; index < gameOptions.size(); ++index) {
            const auto &game = gameOptions[index];
            auto y = index * (regionHeight + gap) + gap + totalOffset;
            bool selected = menuHover == index;

            if (selected) {
                auto width = 900;
                DrawRectangle(95, y, width, regionHeight, DARKBROWN);
                for (size_t i = 0; i < 4; ++i) {
                    DrawRectangleLines(95 - i, y - i, width + (i * 2), regionHeight + (i * 2), YELLOW);
                }
            }
            DrawText(game.name, 450, y + textVOffset, fontSize, selected ? WHITE : GRAY);
            if (gameIcons.contains(game.name)) {
                DrawTexture(gameIcons.at(game.name), 100, y + 5, selected ? WHITE : GRAY);
            }
        }

        EndDrawing();
    }
    else if (IsKeyPressed(KEY_ESCAPE) || IsGamepadButtonPressed(0, GAMEPAD_BUTTON_MIDDLE_RIGHT)) {
        currentGame = nullptr;
        PlayMusicStream(bgMusic);
        SetMusicVolume(bgMusic, 0.25f);
    }
    else {
        currentGame->update();
    }
}

Collection::~Collection() {
    StopMusicStream(bgMusic);
    StopSound(selectChange);
    StopSound(gameSelected);

    UnloadSound(selectChange);
    UnloadSound(gameSelected);
    for (auto& [_, texture] : gameIcons) {
        UnloadTexture(texture);
    }
    UnloadMusicStream(bgMusic);
}

