#pragma once

#include "../common/common.h"
#include <raylib.h>
#include <string>
#include <map>

class Collection {
public:
    Collection();
    void update();
    ~Collection();
private:
    std::unique_ptr<Game> currentGame = nullptr;
    std::vector<GameCreator> gameOptions = get_game_creators();
    std::map<std::string, Texture2D> gameIcons = {};
    int menuHover = 0;
    float timeSinceLastSelectionChange = 0.0f;
    int lastMenuHover = 0;
    Music bgMusic;
    Sound selectChange;
    Sound gameSelected;
};
